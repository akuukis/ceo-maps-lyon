# Get started locally locally.

> FYI: I recommend using VSCode IDE because I've already configured it (open workspace within `./vscode` folder).

1. Install NodeJS
2. Install Yarn
3. `yarn install`
4. `yarn build`
5. Open `./dist/index.html` in your favorite browser.
6. Run `yarn start` and start editing files.

Read `package.json` scripts for everything you most likely need (I bet you want).