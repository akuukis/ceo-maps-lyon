// tslint:disable
const {join, resolve} = require('path')
const webpack = require('webpack')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

const ROOT_DIR = resolve(__dirname)
const SRC_DIR = join(ROOT_DIR, 'src');
const isDev = process.argv.mode === 'development'
const isHot = process.argv.includes('--hot');


module.exports = () => {
    return {
        target: 'web',
        entry: {
            index: [
                join(SRC_DIR, 'index.ts'),
            ],
        },
        output: {
            path: join(ROOT_DIR, 'dist'),
            filename: '[name].js',
        },
        resolve: {
            extensions: [".ts", ".tsx", ".js", ".jsx"],
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: 'ts-loader',
                    options: {
                        transpileOnly: true,  // disable type checker - we will use it in fork plugin
                        compilerOptions: {
                            incremental: false,
                        },
                    },
                    exclude: /node_modules/,
                },
            ]

        },
        plugins: [
            new ForkTsCheckerWebpackPlugin({tsconfig: 'tsconfig.json', silent: process.argv.json}),
            new webpack.NamedModulesPlugin(),
        ],
        externals: [
            'd3',
        ],
        stats: {
            all: undefined,
            assets: isDev,
            assetsSort: "field",
            builtAt: false,
            children: false,
            chunks: true,
            chunkGroups: isDev,
            chunkModules: false,
            chunkOrigins: false,
            chunksSort: "field",
            context: SRC_DIR,
            colors: true,
            depth: false,
            entrypoints: false,
            env: false,
            errors: true,
            errorDetails: false,
            hash: isDev,
            modules: false,
            moduleTrace: true,
            performance: true,
            providedExports: true,
            publicPath: true,
            timings: true,
            version: isDev,
            warnings: true,
            warningsFilter: [/ed25519/],
        },

        devServer: {
            contentBase: join(ROOT_DIR, 'dist'),
            https: false,
            ...(isHot ? {hot: true} : {}),
        }
    }
}
