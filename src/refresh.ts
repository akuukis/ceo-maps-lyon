import { cache, saveAsLameCsv } from './cache'
import { DOMAIN_TYPE, IDomain } from './interfaces'


const myKingdomId = KINGDOM_ID.DEMALION
const myDuchyIds = [
    20401,  // Westmist -> Lyongaard
    20664,  // Stormcombe -> Lyon
    // 20556,
    // 20479,
]

const enum KINGDOM_ID {
    // AL_KHEZAM = 21337,
    // ARKADIA = 22697,
    DEMALION = 27532,
    // NIRATH = 18608,
    // TRYGGR = 17329,
}


(async () => {
    await cache(DOMAIN_TYPE.DUCHY, myKingdomId)

    const duchies: IDomain.IDomainDuchy[] = []
    {
        const {data} = await cache(DOMAIN_TYPE.DUCHY, myKingdomId)
        duchies.push(...data.slice(1) as IDomain.IDomainDuchy[])
        saveAsLameCsv('duchies', duchies)
    }
    saveAsLameCsv('counties', duchies)

    const counties: IDomain.IDomainCounty[] = []
    for(const duchy of duchies) {
        // if(!myDuchyIds.includes(duchy.Id)) continue

        console.info('#  ', duchy.FinalName || duchy.TemporaryName, duchy.ParcelCount)
        const {data} = await cache(DOMAIN_TYPE.COUNTY, duchy.Id)
        if(data === null) continue
        console.info(data.slice(1).map((datum) => `    ${datum.FinalName || datum.TemporaryName} (${datum.ParcelCount})`).join('\n'))
        counties.push(...data.slice(1) as IDomain.IDomainCounty[])
    }
    saveAsLameCsv('counties', counties)

    const settlements: IDomain.IDomainSettlement[] = []
    for(const county of counties) {
        console.info('###', county.TemporaryName, county.ParcelCount)
        const {data} = await cache(DOMAIN_TYPE.SETTLEMENT, county.Id)
        settlements.push(...data.slice(1) as IDomain.IDomainSettlement[])
        console.info(data.slice(1).map((datum) => `    ${datum.FinalName || datum.TemporaryName} (${datum.ParcelCount})`).join('\n'))
    }
    saveAsLameCsv('settlements', settlements)


})()
    .catch((err) => console.error(err))

