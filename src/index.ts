// tslint:disable:no-implicit-dependencies
import * as d3 from 'd3'
import { Feature, Polygon } from 'geojson'

import { AnnotationStore } from './annotation'
import { GeoChroniclesOfElyria } from './GeoCoE'
import { IDomain } from './interfaces'

declare const saveSvgAsPng: any

const MIN_ZOOM = 0.3
const MAX_CUSTOM_ZOOM = 16 * 4
const MAX_AUTO_ZOOM = 16 * 3

const DebugBounds = (path: d3.GeoPath) => (d: Feature<Polygon, IDomain.IDomainDuchy>) => {
    console.info(`${d.properties.TemporaryName}: x=[${path.bounds(d)[0][0]} ~ ${path.bounds(d)[1][0]}], y=[${path.bounds(d)[0][1]} ~ ${path.bounds(d)[1][1]}]`)
}

/**
 * https://stackoverflow.com/a/54572138/4817809
 */
const setHashParam = (name: string, value: string) => {
    const urlObj = new URL(window.location.href)
    const dummyObj = new URL('https://dummy.com')
    dummyObj.search = urlObj.hash.substring(1)
    dummyObj.searchParams.set(name, value)
    urlObj.hash = dummyObj.searchParams.toString()
    window.location.replace(urlObj.href)
}
/**
 * https://stackoverflow.com/a/54572138/4817809
 */
const getHashParam = (name: string) => {
    const urlObj = new URL(window.location.href)
    const dummyObj = new URL('https://dummy.com')
    dummyObj.search = urlObj.hash.substring(1)

    return dummyObj.searchParams.get(name)
}

// tslint:disable-next-line:max-func-args - TODO.
const drawCounties = async (
        coe: GeoChroniclesOfElyria,
        kingdom: d3.Selection<SVGGElement, unknown, null, unknown>,
        onClick: (d: Feature<Polygon, IDomain>, path: d3.GeoPath<unknown, d3.GeoPermissibleObjects>) => void,
        countyId: number,
        ours: boolean,
    ) => {

    const data = await coe.getMerged<IDomain.IDomainDuchy>(countyId, coe)

    const settlementData = await Promise.all(
        data.Features.features.map(async (feature) => coe.getMerged<IDomain.IDomainCounty>(feature.properties.Id, coe)),
    )

    // console.info(countyId, data.ParentsBorder.features[0].properties.TemporaryName, data)
    // console.info(settlementData)

    DebugBounds(coe.path)(data.ParentsBorder.features[0])

    const duchy = kingdom
        .append('g')
        .attr('id', 'county-borders')
        .classed('neighbor', !ours)


    const counties = duchy
        .selectAll('g')
        .data(data.Features.features)
        .enter()
        .append('g')
            .attr('data-id', (d) => d.properties.Id)
            .on('mouseover', (d, i, elements) => d3.select(elements[i]).classed('focus', true).select('text').raise())
            .on('mouseout', (d, i, elements) => d3.select(elements[i]).classed('focus', false))
            .on('click', (d) => onClick(d, coe.path))

    counties.append('path')
        .attr('d', coe.path)

    const asdf = (d: Feature<Polygon, IDomain.IDomainCounty>) => {
        return settlementData.find((sd) => sd.ParentsBorder.features[0].properties.Id === d.properties.Id).Features.features
    }
    const settlemets = counties
        // .datum(asdf)
        .selectAll('g')
        .data(asdf)
        .enter()
        .append('g')
            .classed('settlement', true)
            .attr('data-id', (d) => d.properties.Id)
            .attr('data-x', (d) => Math.round(coe.path2.centroid(d)[0]))
            .attr('data-y', (d) => Math.round(coe.path2.centroid(d)[1]))
            .on('mouseover', (d, i, elements) => d3.select(elements[i]).classed('focus', true).raise())
            .on('mouseout', (d, i, elements) => d3.select(elements[i]).classed('focus', false))
            .on('click', (d) => onClick(d, coe.path))

    settlemets.append('path')
        .attr('d', coe.path)
        // .each((d) => d.properties.ParentDomainId === 20456 && console.info(
        //     (d.properties.FinalName || d.properties.TemporaryName),
        //     coe.path2.area(d),
        //     Number(d.properties.ParcelCount.replace(/\,|\s/, '').split('-')[0]),
        //     `${Math.round(coe.path2.area(d) / Number(d.properties.ParcelCount.replace(/\,|\s/, '').split('-')[0]) * 100)}%`,
        // ))

    counties.append('text')
        .attr('x', (d) => coe.path.centroid(d)[0])
        .attr('y', (d) => coe.path.centroid(d)[1])
        .text((d) => `${(d.properties.FinalName || d.properties.TemporaryName).replace(' ', '\n')}`)
        // .text((d) => `${(d.properties.FinalName || d.properties.TemporaryName).replace(' ', '\n')} (${Math.round(path.area(d) / (4 * 4))})`)

    settlemets.append('text')
        .text((d) => (d.properties.FinalName || d.properties.TemporaryName))
        .attr('x', (d) => coe.path.centroid(d)[0])
        .attr('y', (d) => coe.path.centroid(d)[1])

    // console.info(settlemets)

    return duchy
}


(async () => {
    const svg = d3.select<SVGSVGElement, null>('svg')
    const coe = new GeoChroniclesOfElyria(svg.node())
    const zoom = d3.zoom()
        .scaleExtent([MIN_ZOOM, MAX_CUSTOM_ZOOM])
        .translateExtent([
            [
                Number(coe.rect.attr('x')) - (Number(coe.rect.attr('x')) + Number(coe.rect.attr('width'))) * 0.1,
                Number(coe.rect.attr('y')) - (Number(coe.rect.attr('y')) + Number(coe.rect.attr('height'))) * 0.1,
            ],
            [
                (Number(coe.rect.attr('x')) + Number(coe.rect.attr('width'))) * 1.1,
                (Number(coe.rect.attr('y')) + Number(coe.rect.attr('height'))) * 1.1,
            ],
        ])
        .on('zoom', () => {
            const {transform} = d3.event as d3.D3ZoomEvent<Element, unknown>
            const zPrecision = transform.k >= 1 ? 0 : 1
            setHashParam('zoom', transform.k.toFixed(zPrecision))
            const parcels = [
                -transform.x / transform.k,
                -transform.y / transform.k,
            ]
            const xyPrecision = transform.k >= 10 ? 1 : 0
            setHashParam('x', parcels[0].toFixed(xyPrecision))
            setHashParam('y', parcels[1].toFixed(xyPrecision))
            coe.g.attr('transform', transform.toString())
            coe.g.attr('stroke-width', 1 / transform.k)
        })

    // FYI: sea color is "#B4E1E5".
    coe.addBackground('Selene-8.png', [[7 * 100, -2 * 100], [23 * 100, 28 * 100]])
    coe.addBackground('Demalion-2.png', [[8 * 100, -2 * 100], [21 * 100, 8 * 100]])
    // coe.addBackground('LyonGrid.png', [[845, 30], [1115, 454]])

    svg
        .on('click', () => {
            svg
                .transition()
                .duration(500)
                .call(
                    zoom.scaleTo,  // tslint:disable-line: no-unbound-method
                    MIN_ZOOM,
                )
            })

    svg.call(zoom)

    if(getHashParam('zoom')) {
        const initCoordsPixels = [
            Number(getHashParam('x')) || 850,
            Number(getHashParam('y')) || 50,
        ]
        svg.call(
            zoom.transform,
            d3.zoomIdentity
                .scale(Number(getHashParam('zoom') || MIN_ZOOM))
                .translate(-initCoordsPixels[0], -initCoordsPixels[1]),
        )
    } else {
        zoom.scaleTo(svg, MIN_ZOOM)
        zoom.translateTo(
            svg,
            (Number(coe.rect.attr('x')) + Number(coe.rect.attr('width'))) / 2,
            (Number(coe.rect.attr('y')) + Number(coe.rect.attr('height'))) / 2,
        )
    }

    const onClick = (d: Feature<Polygon, IDomain>, path: d3.GeoPath<unknown, d3.GeoPermissibleObjects>) => {
        const [[x0, y0], [x1, y1]] = path.bounds(d)
        const width = Number(svg.node().clientWidth)
        const height = Number(svg.node().clientHeight)
        ;
        (d3.event as Event).stopPropagation()
        svg
            .transition()
            .duration(750)
            .call(
                zoom.transform,  // tslint:disable-line: no-unbound-method
                d3.zoomIdentity
                    .translate(width / 2, height / 2)
                    .scale(Math.min(MAX_AUTO_ZOOM, 0.8 / Math.max((x1 - x0) / width, (y1 - y0) / height)))
                    .translate(-(x0 + x1) / 2, -(y0 + y1) / 2),
                d3.mouse(svg.node()),
            )
    }
    const duchyIds = [
        // 16207,  // Silverbow Vale
        // 16284,  // The Barn
        // 16373,  // East Valor
        // 16561,  // Eastale
        // 16645,  // Redphoenix Cove
        // 16936,  // Redshore
        // 20257,  // Blackvale
        // 20327,  // Stormharvest Bridge
        20401,  // Westmist
        // 20479,  // Forestanchor
        // 20556,  // Greyspring
        20664,  // Stormcombe
        // 20758,  // Longstone
        // 20847,  // Hazelhorn
        // 20947,  // Stormhollow
        // 21236,  // Oakbone Heights
        // 27541,  // Whitehollow
        // 27578,  // Jewelmere
        // 27585,  // Darkbaien Heights
        // 27640,  // Wolfside
    ]
    for(const duchyId of duchyIds) await drawCounties(coe, coe.g, onClick, duchyId, true)  // Westmist

    if(getHashParam('userdata')) {
        const userdatas = getHashParam('userdata').split(',')
        for(const userdata of userdatas) {
            const annotationStore = await AnnotationStore.new(coe.g.node(), coe, userdata)
            // annotationStore.update()
        }
    }

    // const scale = 8
    // const node2 = svg.node().cloneNode(true) as SVGSVGElement
    // const n2 = d3.select(node2)
    //     .attr('height', 1000 * scale)
    //     .attr('width', 1300 * scale)
    //     .attr('style', 'transform: translate3d(0px, 0px, 0px)')
    // n2.select('g')
    //     .attr('transform', `translate(${-800 * scale}, ${200 * scale}) scale(${scale})`)
    // await saveSvgAsPng(n2.node(), 'map.png', {
    //     height: 1000 * scale,
    //     width: 1300 * scale,
    // })

    console.info('Done!')
})()
    .catch((err) => console.error(err))
