import { FeatureCollection, Polygon } from 'geojson'


export enum RESOURCE_NAME {
    CAVES = 'Caves',
    CLAY = 'Clay',
    COASTAL_WATER = 'Coastal Water',
    FARMLAND = 'Farmland',
    FISH = 'Fish',
    FORAGING = 'Foraging',
    FRESH_WATER = 'Fresh Water',
    GAME = 'Game',
    HIGH_GROUND = 'High Ground',
    MINERAL_DEPOSITS = 'Mineral Deposits',
    SAND = 'Sand',
    STONE = 'Stone',
    TRADE_ROUTE = 'Trade Route',
    WOOD = 'Wood',
}

export enum TRIBE_NAME {
    HROTHI = 'Hrothi',
    BRUDVIR = 'Brudvir',
    NERAN = 'Neran',
    KYPIQ = 'Kypiq',
    THE_WAERD = 'The Waerd',
}

export enum DOMAIN_TYPE {
    KINGDOM = 4,
    DUCHY = 3,
    COUNTY = 2,
    SETTLEMENT = 1,
}

export enum BIOME {
    ALPINE_CONIFEROUS_FOREST = 'Alpine Coniferous Forest',
    ALPINE_TUNDRA = 'Alpine Tundra',
    LOWER_MONTANE_FOREST = 'Lower Montane Forest',
    TAIGA = 'Taiga',
}

export enum WEALTH {
    RICH = 'Rich',
    AVERAGE = 'Average',
    POOR = 'Poor',
}

export enum SETTLEMENT_TYPE {
    NONE = 0,
    UNKNOWN = 1,
}

export enum SETTLEMENT_SIZE_CLASS {
    HAMLET = 0,
    VILLAGE = 1,
    TOWN = 2,
    CITY = 3,
}

export enum SERVER_ID {
    LUNA = 3,
}

export enum DOMAIN_STATUS {
    UNCLAIMED = 0,
    CLAIMED = 2,
}

export interface IDistribution<TName extends RESOURCE_NAME | TRIBE_NAME> {
    'Distribution': number,
    'Id': number,
    'Name': TName,
}

export interface ISubdomains {
    'County'?: number,
    'Duchy'?: number,
    'Settlement'?: number,
}

export interface IDomainNeighbour {
    'Adjacent': boolean,
    'Id': number,
}

/**
 * TODO: Enumerate.
 */
export type Profession = string
export type ProfessionX5 = [Profession, Profession, Profession, Profession, Profession]
export type ProfessionX10 = [Profession, Profession, Profession, Profession, Profession, Profession, Profession, Profession, Profession, Profession]

export type IDomain =
    | IDomain.IDomainKingdom
    | IDomain.IDomainDuchy
    | IDomain.IDomainCounty
    | IDomain.IDomainSettlement

export namespace IDomain {
    export interface IDomainBase {
        AvailableSubDomains: ISubdomains
        Biomes: BIOME[],
        Code: string,
        Constructions: null | unknown,
        DomainType: DOMAIN_TYPE,
        FinalName: string,
        Focus: null | string,
        Id: number,
        IsClaimed: boolean,
        IsCoastal: boolean,
        IsDefensive: boolean,
        IsFreeKingdom: boolean,
        IsOver25: boolean,
        Location: [number, number],  // Equals IGeoSettlement.LatLong.
        MultiDomains: null | number,
        Neighbours: IDomainNeighbour[],
        NotableProfessions: ProfessionX5 | ProfessionX10,
        OpenSlotsAvailable: number,
        OwnedByCount: boolean,
        ParcelCount: string,  // '2,192 - 2,965'  OR  '64 - 87' in km2
        ParentDomainId: null | number,
        Population: string,  // '73,711-99,328'
        Professions: ProfessionX5 | ProfessionX10,
        Resources: Array<IDistribution<RESOURCE_NAME>>,
        Roles: null | unknown,
        ServerId: SERVER_ID,
        SettlementCount: number,
        SettlementType: 0 | SETTLEMENT_TYPE,
        SizeClass: null | SETTLEMENT_SIZE_CLASS,
        Status: DOMAIN_STATUS,
        Sustainability: null | number,
        TemporaryName: string,
        TotalSubDomains: ISubdomains,
        Tribes: Array<IDistribution<TRIBE_NAME>>,
        Wealth: WEALTH,
        ZoomLevel: 0,
    }

    export interface IDomainSettlement extends IDomainBase {
        DomainType: DOMAIN_TYPE.SETTLEMENT,
        Focus: string,
        NotableProfessions: ProfessionX5,
        ParentDomainId: number,
        Professions: ProfessionX5,
        SettlementType: SETTLEMENT_TYPE,
        SizeClass: SETTLEMENT_SIZE_CLASS,
        Sustainability: number,
    }

    export interface IDomainCounty extends IDomainBase {
        DomainType: DOMAIN_TYPE.COUNTY,
        Focus: null,
        NotableProfessions: ProfessionX5,
        ParentDomainId: number,
        Professions: ProfessionX5,
        SettlementType: 0,
        SizeClass: null,
        Sustainability: null,
    }

    export interface IDomainDuchy extends IDomainBase {
        DomainType: DOMAIN_TYPE.DUCHY,
        Focus: null,
        NotableProfessions: ProfessionX10,
        ParentDomainId: number,
        Professions: ProfessionX10,
        SettlementType: 0,
        SizeClass: null,
        Sustainability: null,
    }

    export interface IDomainKingdom extends IDomainBase {
        DomainType: DOMAIN_TYPE.KINGDOM,
        Focus: null,
        NotableProfessions: ProfessionX10,
        ParentDomainId: null,
        Professions: ProfessionX10,
        SettlementType: 0,
        SizeClass: null,
        Sustainability: null,
    }
}


export interface IGeoSettlement {
    Id: number,
    IsCoastal: boolean
    IsDefensive: boolean,
    IsEnabled: boolean,
    LatLng: [number, number],  // Equals IDomain.Location.
    Name: string,
    SettlementType: SETTLEMENT_TYPE,
    SizeClass: SETTLEMENT_SIZE_CLASS,
}

export interface IGeoProperties {
    center: [number, number],  // Equals IGeoSettlementLatLong & IDomain.Location.
    id: number,  // Equals IDomain.Id.
}

export interface IGeoEBProperties {
    id: number,
    isClaimed: boolean,
    isParent: boolean,
    nameParent: string,
    parentId: number,
    sameGrandParent: boolean,
}

export interface IGeo {
    Bounds: [[number, number], [number, number]],
    ExternalBorders: FeatureCollection<Polygon, IGeoEBProperties>,
    Features: FeatureCollection<Polygon, IGeoProperties>,  // That's my children.
    PanCoordinates: [number, number],
    ParentsBorder: FeatureCollection<Polygon, {id: number}>,  // That's me.
    Settlements: IGeoSettlement[],
}

export type SubDomain<TDomain extends IDomain> =
    TDomain extends IDomain.IDomainKingdom ? IDomain.IDomainDuchy :
    TDomain extends IDomain.IDomainDuchy ? IDomain.IDomainCounty :
    TDomain extends IDomain.IDomainCounty ? IDomain.IDomainSettlement :
    never

export interface IMerged<TDomain extends IDomain = IDomain> {
    Bounds: [[number, number], [number, number]],
    ExternalBorders: FeatureCollection<Polygon, IGeoEBProperties>,
    Features: FeatureCollection<Polygon, SubDomain<TDomain>>,  // That's my children.
    PanCoordinates: [number, number],
    ParentsBorder: FeatureCollection<Polygon, TDomain>,  // That's me. Always only one feature.
    Settlements: IGeoSettlement[],
}
