import { lstatSync, mkdirSync, readFileSync, writeFileSync } from 'fs'
import fetch from 'node-fetch'
import { join } from 'path'

import { DOMAIN_TYPE, IDomain } from './interfaces'


const dataUrl = (domain: DOMAIN_TYPE, parent: number) => `https://chroniclesofelyria.com/domains/api/domain/3/${domain}?parentIds%5B0%5D=${parent}`
const geoUrl = (domain: DOMAIN_TYPE, parent: number) => `https://chroniclesofelyria.com/domains/api/domain/geo/3/${domain}?parentIds%5B0%5D=${parent}`

const mkdirp = (dir: string) => {
    try {
        mkdirSync(dir)
    } catch(err) {
        // Do nothing.
    }
}

export const cache = async (domain: DOMAIN_TYPE, id: number): Promise<{data: IDomain[], geo: unknown}> => {
    mkdirp('tmp')
    const filenameData = join('tmp', `${id}-data.json`)
    const filenameGeo = join('tmp', `${id}-geo.json`)
    try {
        // Skip downloading again.
        lstatSync(filenameData)
        lstatSync(filenameGeo)

        return {
            data: JSON.parse(readFileSync(filenameData).toString('utf-8')) as IDomain[],
            geo: JSON.parse(readFileSync(filenameGeo).toString('utf-8')) as unknown,
        }
    } catch(err) {
        const data = await (await fetch(dataUrl(domain, id))).text()
        writeFileSync(join(filenameData), data)
        const geo = await (await fetch(geoUrl(domain, id))).text()
        writeFileSync(filenameGeo, geo)

        return {
            data: JSON.parse(data) as IDomain[],
            geo: JSON.parse(geo) as unknown,
        }
    }
}

export const saveAsLameCsv = (filename: string, data: IDomain[]) => {
    const SEPERATOR = ';'
    const keys = Object.keys(data[0]) as Array<keyof IDomain>
    const lines: string[] = []
    for(const datum of data) lines.push(keys.map((key) => datum[key] === undefined ? '' : JSON.stringify(datum[key])).join(SEPERATOR))

    const content = `${keys.join(SEPERATOR)}\n${lines.join('\n')}`
    writeFileSync(join('tmp', `${filename}.csv`), content)
}
