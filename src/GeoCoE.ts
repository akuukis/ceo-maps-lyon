// tslint:disable:no-implicit-dependencies
import * as d3 from 'd3'
import { Feature, Polygon } from 'geojson'

import { IDomain, IGeo, IMerged, SubDomain } from './interfaces'


export const getPoint = (center: [number, number], i: number, radius: number) => {
    const radians = i * 10 * Math.PI / 180
    const x = center[0] + radius * Math.cos(radians)
    const y = center[1] + radius * Math.sin(radians)

    return [x, y]
}

// tslint:disable-next-line: min-class-cohesion
export class GeoChroniclesOfElyria {
    public readonly g: d3.Selection<SVGGElement, unknown, null, undefined>
    public readonly rect: d3.Selection<SVGGElement, unknown, null, undefined>
    public readonly path: d3.GeoPath<unknown, d3.GeoPermissibleObjects>
    public readonly path2: d3.GeoPath<unknown, d3.GeoPermissibleObjects>
    public readonly projectionLatLongToParcels: ReturnType<typeof d3.geoTransform>
    public readonly projectionLatLongToPixels: ReturnType<typeof d3.geoTransform>
    public readonly size: [number, number]

    /**
     * SbS constant - they use geo coordinates, where 1/16 point equals 1 parcel.
     */
    public readonly PARCELS_PER_LATLNG = 16
    public readonly STRETCH_X = -0.5
    public readonly STRETCH_Y = 0

    public constructor(svg: SVGSVGElement) {
        this.g = d3.select(svg).append('g')

        this.rect = this.g.append('rect')
            .attr('id', 'frame')
            .attr('x', this.PARCELS_PER_LATLNG * -1 * 90 * this.STRETCH_X)
            .attr('y', this.PARCELS_PER_LATLNG * -1 * 90 * this.STRETCH_Y)
            .attr('width', this.PARCELS_PER_LATLNG * 90 * (this.STRETCH_X * 2 + 2))
            .attr('height', this.PARCELS_PER_LATLNG * 90 * (this.STRETCH_Y * 2 + 2))
            .attr('fill', '#FFF0')

        // tslint:disable-next-line: no-this-assignment
        const self = this

        this.projectionLatLongToParcels = d3.geoTransform({
            point(x, y) {
                this.stream.point(x, y)  // tslint:disable-line: no-unsafe-any - bug?
            },
        })
        this.projectionLatLongToPixels = d3.geoTransform({
            point(x, y) {
                const coords = self.fromLatLngToParcels([x, y])
                this.stream.point(coords[0], coords[1])  // tslint:disable-line: no-unsafe-any - bug?
            },
        })
        this.path = d3.geoPath().projection(this.projectionLatLongToPixels)
        this.path2 = d3.geoPath().projection(this.projectionLatLongToParcels)
    }

    public addBackground(link: string, bounds: [[number, number], [number, number]]) {
        const img = this.g.append('image')  // tslint:disable-line: no-dead-store
            .attr('xlink:href', link)
            .attr('fill', '#aadfe2')
            .attr('x', bounds[0][0])
            .attr('y', bounds[0][1])
            .attr('width', bounds[1][0] - bounds[0][0])
            .attr('height', bounds[1][1] - bounds[0][1])
    }
    public fromLatLngToParcels = (coords: [number, number]): [number, number] => {
        return [
            (1 * coords[0] + 90) * this.PARCELS_PER_LATLNG,
            (-1 * coords[1] + 90) * this.PARCELS_PER_LATLNG,
        ]
    }

    public fromParcelsToLatLng = (coords: [number, number]): [number, number] => {
        return [
            +(coords[0] / this.PARCELS_PER_LATLNG - 90),
            -(coords[1] / this.PARCELS_PER_LATLNG - 90),
        ]
    }


    public geoFlatCircle(centerLatLong: [number, number], radiusInParcels: number): Polygon {
        const radius = radiusInParcels / this.PARCELS_PER_LATLNG
        const data = []

        for (let i = 0; i < 37; i += 1) {
            const p = getPoint(centerLatLong, i, radius)
            data.push ([ p[0], p[1] ])
        }

        return {
            coordinates: [data],
            type: 'Polygon',
        }
    }

    public async getMerged<T extends IDomain>(id: number, ceo: GeoChroniclesOfElyria): Promise<IMerged<T>> {
        const geo = await d3.json<IGeo>(`sbs/${id}-geo.json`)

        const allProperties = await d3.json<IDomain[]>(`sbs/${id}-data.json`)
        const myProperties = allProperties[0] as T
        const subdomainProperties = allProperties.slice(1) as Array<SubDomain<T>>

        let features: IMerged<T>['Features']['features']
        if(geo.Features.features.length === 0) {
            // I am a county, add settlements as circle features.
            features = await Promise.all(geo.Settlements.map<Promise<Feature<Polygon, SubDomain<T>>>>(async (settlement) => {
                const properties = subdomainProperties.find((prop) => prop.Id === settlement.Id)
                const minParcels = Number(properties.ParcelCount.replace(/\,|\s/, '').split('-')[0])
                const radius = Math.sqrt(minParcels / Math.PI)
                const circle = this.geoFlatCircle([settlement.LatLng[1], settlement.LatLng[0]], radius)

                return {
                    geometry: circle,
                    properties,
                    type: 'Feature',
                }
            }))
        } else {
            // I am a kingdom/duchy, subdomain already has features. Add only props.
            features = await Promise.all(geo.Features.features.map(async (ge) => ({
                    ...ge,
                    properties: subdomainProperties.find((prop) => prop.Id === ge.properties.id),
                })))
        }

        return {
            ...geo,
            Features: {
                ...geo.Features,
                features,
            },
            ParentsBorder: {
                ...geo.ParentsBorder,
                features: [{
                    ...geo.ParentsBorder.features[0],
                    properties: myProperties,
                }],
            },
        }

    }

}
