// tslint:disable:no-implicit-dependencies
import * as d3 from 'd3'

import { GeoChroniclesOfElyria } from '../GeoCoE'
import { AbstractAnnotation, Annotation } from './Annotation'

export interface IAnnotationIconProps extends Annotation.IProps {
    [Annotation.PROPS.TYPE]: Annotation.TYPE.LINE
}


export class AnnotationIcon extends AbstractAnnotation<Annotation.TYPE.ICON> {

    public static update(ref: SVGElement, dataRaw: AnnotationIcon[], coe: GeoChroniclesOfElyria) {
        const data = dataRaw.map((datum) => {
            const properties = {
                ...datum,
                center: [
                    datum.coordinates.x,
                    datum.coordinates.y,
                ],
            }

            // tslint:disable-next-line: no-object-literal-type-assertion
            return {
                properties,
            }
        })

        const texts = d3.select(ref).append<Element>('g')
            .classed(`annotation`, true)
            .classed(Annotation.TYPE.ICON, true)
            .selectAll('image')
            .data(data)
            .enter()
            .append('image')
                .attr('x', (d) => d.properties.center[0])
                .attr('y', (d) => d.properties.center[1])
                .attr('width', (d) => d.properties.size[0])
                .attr('height', (d) => d.properties.size[1])
                .attr('preserveAspectRatio', 'none')
                .attr('href', (d) => d.properties.href)
                .attr('opacity', (d) => d.properties.opacity || null)
                .attr('style', (d) => [
                    d.properties.color ? `filter: invert(.5) sepia(1) saturate(5) hue-rotate(${d.properties.color}deg);` : '',
                    d.properties.style ? d.properties.style : '',
                ].join(''))
                .attr('color', (d) => d.properties.color)
                .attr('stroke-width', (d) => d.properties.strokeWidth ? d.properties.strokeWidth : null)
                .attr('stroke', (d) => d.properties.stroke)
                .attr('transform', (d) => {
                    const rotationCenter = [
                        d.properties.center[0] + d.properties.size[0] / 2,
                        d.properties.center[1] + d.properties.size[1] / 2,
                    ]
                    const rotate = d.properties.rotation ? `rotate(${d.properties.rotation} ${rotationCenter[0]} ${rotationCenter[1]})` : ''
                    const translate = `translate(-${d.properties.size[0] / 2} -${d.properties.size[1] / 2})`

                    return `${translate} ${rotate}`
                })

    }

    public coordinates: {x: number, y: number}
    public size: [number, number]

    public constructor(datum: Annotation.IProps) {
        super(Annotation.TYPE.ICON, datum)

        // TODO: Validation.
        this.size = [
            Number(datum[Annotation.PROPS.SIZE].split('/')[0]),
            Number(datum[Annotation.PROPS.SIZE].split('/')[1]),
        ]

        const coordsString = datum[Annotation.PROPS.COORDINATES].split('/')
        this.coordinates = {
            x: Number(coordsString[0]),
            y: Number(coordsString[1]),
        }
    }
}
