// tslint:disable:no-implicit-dependencies
import * as d3 from 'd3'
import { Feature, Polygon } from 'geojson'

import { GeoChroniclesOfElyria } from '../GeoCoE'
import { AbstractAnnotation, Annotation } from './Annotation'

export interface IAnnotationPointProps extends Annotation.IProps {
    [Annotation.PROPS.TYPE]: Annotation.TYPE.POINT
}


export class AnnotationPoint extends AbstractAnnotation<Annotation.TYPE.POINT> {

    public static update(ref: SVGElement, dataRaw: AnnotationPoint[], coe: GeoChroniclesOfElyria) {
        const data = dataRaw.map((datum) => {
            const latLong = coe.fromParcelsToLatLng([datum.coordinates.x, datum.coordinates.y])
            const geometry = coe.geoFlatCircle(latLong, datum.size)
            const properties = {
                ...datum,
                center: coe.path.centroid(geometry),
            }

            // tslint:disable-next-line: no-object-literal-type-assertion
            return {
                geometry,
                properties,
                type: 'Feature',
            } as Feature<Polygon, typeof properties>
        })

        const points = d3.select(ref).append<Element>('g')
            .classed(`annotation`, true)
            .classed(Annotation.TYPE.POINT, true)
            .selectAll('path')
            .data(data)
            .enter()
            .append('path')
                .attr('x', (d) => d.properties.center[0])
                .attr('y', (d) => d.properties.center[1])
                .attr('d', coe.path)
                .attr('opacity', (d) => d.properties.opacity || null)
                .attr('fill', (d) => d.properties.color)
                .attr('stroke-width', (d) => d.properties.strokeWidth)
                .attr('stroke', (d) => d.properties.stroke)
                .attr('style', (d) => d.properties.style ? d.properties.style : null)

    }

    public coordinates: {x: number, y: number}
    public size: number

    public constructor(datum: Annotation.IProps) {
        super(Annotation.TYPE.POINT, datum)

        // TODO: Validation.
        const coordsString = datum[Annotation.PROPS.COORDINATES].split('/')
        this.coordinates = {
            x: Number(coordsString[0]),
            y: Number(coordsString[1]),
        }
    }
}
