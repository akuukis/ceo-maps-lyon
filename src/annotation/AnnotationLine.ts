// tslint:disable:no-implicit-dependencies
import * as d3 from 'd3'
import { Feature, LineString } from 'geojson'

import { GeoChroniclesOfElyria } from '../GeoCoE'
import { AbstractAnnotation, Annotation } from './Annotation'

export interface IAnnotationLineProps extends Annotation.IProps {
    [Annotation.PROPS.TYPE]: Annotation.TYPE.LINE
}


export class AnnotationLine extends AbstractAnnotation<Annotation.TYPE.LINE> {

    public static update(ref: SVGElement, dataRaw: AnnotationLine[], coe: GeoChroniclesOfElyria) {
        const data = dataRaw.map((datum) => {
            const geometry: LineString = {
                coordinates: datum.coordinates.map(coe.fromParcelsToLatLng),
                type: 'LineString',
            }
            const properties = {
                ...datum,
                center: coe.path.centroid(geometry),
            }

            // tslint:disable-next-line: no-object-literal-type-assertion
            return {
                geometry,
                properties,
                type: 'Feature',
            } as Feature<LineString, typeof properties>
        })

        const lines = d3.select(ref).append<Element>('g')
            .classed(`annotation`, true)
            .classed(Annotation.TYPE.LINE, true)
            .selectAll('path')
            .data(data)
            .enter()
            .append('path')
                .attr('x', (d) => d.properties.center[0])
                .attr('y', (d) => d.properties.center[1])
                .attr('d', coe.path)
                .attr('opacity', (d) => d.properties.opacity || null)
                .attr('fill', (d) => d.properties.color)
                .attr('stroke-width', (d) => d.properties.strokeWidth)
                .attr('stroke', (d) => d.properties.stroke)
                .attr('style', (d) => d.properties.style ? d.properties.style : null)

    }

    public coordinates: Array<[number, number]>

    public constructor(datum: Annotation.IProps) {
        super(Annotation.TYPE.LINE, datum)

        // TODO: Validation.
        this.coordinates = datum[Annotation.PROPS.COORDINATES]
            .replace(/\s+/g, ' ')
            .split(' ')
                .map((inner) => [Number(inner.split('/')[0]), Number(inner.split('/')[1])])
    }
}
