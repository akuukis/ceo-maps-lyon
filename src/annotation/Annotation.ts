// tslint:disable:no-implicit-dependencies
import { GeoChroniclesOfElyria } from '../GeoCoE'


export namespace Annotation {
    export type Integer = string
    export type Float = string
    export type Color = string
    export type Scale = string

    export enum TYPE {
        IGNORE = '',
        POINT = 'point',
        TEXT = 'text',
        LINE = 'line',
        ICON = 'icon',
    }

    export enum PROPS {
        TYPE,
        TITLE,
        COORDINATES,
        SIZE,
        ROTATION,
        HREF,
        NOTES,
        OPACITY,
        COLOR,
        STROKE_WIDTH,
        STROKE,
        STYLE,
    }

    export interface IProps {
        [PROPS.TYPE]: TYPE
        [PROPS.TITLE]: string
        [PROPS.COORDINATES]: string
        [PROPS.SIZE]: string
        [PROPS.ROTATION]: string
        [PROPS.HREF]: string
        [PROPS.NOTES]: string
        [PROPS.OPACITY]: string
        [PROPS.COLOR]: string
        [PROPS.STROKE_WIDTH]: string
        [PROPS.STROKE]: string
        [PROPS.STYLE]: string
    }
}


// tslint:disable-next-line: min-class-cohesion
export abstract class AbstractAnnotation<T extends Annotation.TYPE = Annotation.TYPE> {

    public static update(ref: SVGElement, data: AbstractAnnotation[], coe: GeoChroniclesOfElyria) {
        console.info(data)
    }

    public color?: string
    public coordinates: unknown
    public href?: string
    public notes?: never
    public opacity?: number
    public rotation?: string
    public size?: number | [number, number]
    public stroke?: string
    public strokeWidth?: number
    public style?: string

    public title: string
    public type: T

    public constructor(type: T, datum: Annotation.IProps) {
        this.type = type

        // TODO: Validation.
        this.color = datum[Annotation.PROPS.COLOR]
        this.coordinates = datum[Annotation.PROPS.COORDINATES]
        this.href = datum[Annotation.PROPS.HREF]
        this.opacity = Number(datum[Annotation.PROPS.OPACITY])
        this.rotation = datum[Annotation.PROPS.ROTATION]
        this.size = Number(datum[Annotation.PROPS.SIZE])
        this.stroke = datum[Annotation.PROPS.STROKE]
        this.strokeWidth = Number(datum[Annotation.PROPS.STROKE_WIDTH])
        this.style = datum[Annotation.PROPS.STYLE]
        this.title = datum[Annotation.PROPS.TITLE]
    }
}
