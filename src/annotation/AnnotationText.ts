// tslint:disable:no-implicit-dependencies
import * as d3 from 'd3'
import { Feature, Point } from 'geojson'

import { GeoChroniclesOfElyria } from '../GeoCoE'
import { AbstractAnnotation, Annotation } from './Annotation'

export interface IAnnotationTextProps extends Annotation.IProps {
    [Annotation.PROPS.TYPE]: Annotation.TYPE.LINE
}


export class AnnotationText extends AbstractAnnotation<Annotation.TYPE.TEXT> {

    public static update(ref: SVGElement, dataRaw: AnnotationText[], coe: GeoChroniclesOfElyria) {
        const data = dataRaw.map((datum) => {
            const geometry: Point = {
                coordinates: coe.fromParcelsToLatLng([datum.coordinates.x, datum.coordinates.y]),
                type: 'Point',
            }
            const properties = {
                ...datum,
                center: coe.path.centroid(geometry),
            }

            // tslint:disable-next-line: no-object-literal-type-assertion
            return {
                geometry,
                properties,
                type: 'Feature',
            } as Feature<Point, typeof properties>
        })

        const texts = d3.select(ref).append<Element>('g')
            .classed(`annotation`, true)
            .classed(Annotation.TYPE.TEXT, true)
            .selectAll('text')
            .data(data)
            .enter()
            .append('text')
                .attr('x', (d) => d.properties.center[0])
                .attr('y', (d) => d.properties.center[1])
                .text((d) => d.properties.title)
                .attr('opacity', (d) => d.properties.opacity || null)
                .attr('style', (d) => [
                    `font-size: ${d.properties.size};`,
                    `font-family: "${d.properties.href}";`,
                    d.properties.style ? d.properties.style : '',
                ].join(''))
                .attr('fill', (d) => d.properties.color)
                .attr('stroke-width', (d) => d.properties.strokeWidth ? d.properties.strokeWidth : null)
                .attr('stroke', (d) => d.properties.stroke)
                .attr('transform', (d) => {
                    const rotate = d.properties.rotation ? `rotate(${d.properties.rotation} ${d.properties.center[0]} ${d.properties.center[1]})` : ''

                    return `translate(-${d.properties.size / 2} -${d.properties.size / 2}) ${rotate}`
                })

    }

    public coordinates: {x: number, y: number}
    public size: number

    public constructor(datum: Annotation.IProps) {
        super(Annotation.TYPE.TEXT, datum)

        // TODO: Validation.
        const coordsString = datum[Annotation.PROPS.COORDINATES].split('/')
        this.coordinates = {
            x: Number(coordsString[0]),
            y: Number(coordsString[1]),
        }
    }
}
