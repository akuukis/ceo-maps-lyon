import { GeoChroniclesOfElyria } from '../GeoCoE'
import { Annotation } from './Annotation'
import { AnnotationIcon } from './AnnotationIcon'
import { AnnotationLine } from './AnnotationLine'
import { AnnotationPoint } from './AnnotationPoint'
import { AnnotationText } from './AnnotationText'

const API_KEY = 'AIzaSyDkpwEcVl5LFcyFhJB58tCXbIf0MqWLjxI'
const ANNOTATION_SHEET = '_ANNOTATION_'


type ISheetsApi =
    | ISheetsApi.ISheetsApiError
    | ISheetsApi.ISheetsApiValues
namespace ISheetsApi {
    export interface ISheetsApiError {
        error: unknown
        values: never
    }
    export interface ISheetsApiValues {
        error: never
        values: Annotation.IProps[]
    }
}


interface IAnnotationDataContainer {
    icons: AnnotationIcon[]
    lines: AnnotationLine[]
    points: AnnotationPoint[]
    texts: AnnotationText[]
}


export class AnnotationStore {
    public static async new(ref: SVGElement, coe: GeoChroniclesOfElyria, sheetId: string) {
        const annotation = new AnnotationStore(ref, coe, sheetId)
        await annotation.refresh()
        annotation.update()

        return annotation
    }
    protected readonly coe: GeoChroniclesOfElyria
    protected data: IAnnotationDataContainer
    protected readonly ref: SVGElement
    protected readonly sheetId: string

    public constructor(ref: SVGElement, coe: GeoChroniclesOfElyria, sheetId: string) {
        this.ref = ref
        this.coe = coe
        this.sheetId = sheetId
        this.data = {
            lines: [],
            points: [],
            texts: [],
            icons: [],
        }
    }

    public async refresh() {
        const res = await fetch(`https://sheets.googleapis.com/v4/spreadsheets/${this.sheetId}/values/${ANNOTATION_SHEET}!A3:z999?key=${API_KEY}`)
        const {values, error} = await res.json() as ISheetsApi
        if(error) {
            console.error(error)

            return
        }


        const parsed = values.map((prop, i) => {
                switch(prop[Annotation.PROPS.TYPE]) {
                    case(undefined): return null
                    case(Annotation.TYPE.IGNORE): return null
                    case(Annotation.TYPE.POINT): return new AnnotationPoint(prop)
                    case(Annotation.TYPE.LINE): return new AnnotationLine(prop)
                    case(Annotation.TYPE.TEXT): return new AnnotationText(prop)
                    case(Annotation.TYPE.ICON): return new AnnotationIcon(prop)
                    default: {
                        console.warn(`Line ${i + 2}: Unknown type ${prop[Annotation.PROPS.TYPE]}, ignoring...`)

                        return null
                    }
                }
            })
            .filter((annotationOrNull) => !!annotationOrNull)

        this.data.points = parsed.filter((annotation): annotation is AnnotationPoint => annotation instanceof AnnotationPoint)
        this.data.lines = parsed.filter((annotation): annotation is AnnotationLine => annotation instanceof AnnotationLine)
        this.data.texts = parsed.filter((annotation): annotation is AnnotationText => annotation instanceof AnnotationText)
        this.data.icons = parsed.filter((annotation): annotation is AnnotationIcon => annotation instanceof AnnotationIcon)
    }

    public update() {
        AnnotationLine.update(this.ref, this.data.lines, this.coe)
        AnnotationPoint.update(this.ref, this.data.points, this.coe)
        AnnotationIcon.update(this.ref, this.data.icons, this.coe)
        AnnotationText.update(this.ref, this.data.texts, this.coe)
        console.log(`Annotation (${this.sheetId.slice(0, 7)}..):`, this.data)
    }
}
