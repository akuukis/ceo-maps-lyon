# Chronicles of Elyria Maps (Selene > Demalion > Lyon)

View here http://coe-maps-lyon.s3-website-eu-west-1.amazonaws.com/

## Contributing

Want to help? Great! Just ping me on Discord (Akuukis#6154) and I will help out. Also see roadmap below.

Technical people, please refer to [CONTRIBUTION.md](./Contributing.md) for technical details.

## Roadmap (in somewhat priority)

1. annotate duchies, counties with titles and owners
2. add settlements
3. add data overlays at county level (wealth, population, etc.)
4. add data overlays at settlement level (wealth, population, etc.)
5. refactor whole codebase into React+Webpack once it becomes complex (should be around this step)
